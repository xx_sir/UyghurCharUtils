# UyghurCharUtils

#### 项目介绍
维吾尔语基本区和扩展区转换函数类库

#### 使用说明

1. Basic2Extend(source){}       基本区 转换 扩展区
2. Extend2Basic(source){}       扩展区 转换 基本区
3. Basic2RExtend(source){}      基本区 转换 反向扩展区
4. RExtend2Basic(source){}      反向扩展区 转换 基本区
